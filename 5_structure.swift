enum Suit: String {
    case spades = "♠️"
    case hearts = "♥️"
    case diamonds = "♦️"
    case clubs = "♣️"
}

enum Rank: Int, CustomStringConvertible {
    case ace = 1
    case two, three, four, five, six, seven, eight, nine, ten
    case jack, queen, king
    
    var description: String {
        switch self {
        case .ace:
            return "A"
        case .jack:
            return "J"
        case .queen:
            return "Q"
        case .king:
            return "K"
        default:
            return "\(self.rawValue)"
        }
    }
}

struct Card: CustomStringConvertible {
    var rank: Rank
    var suit: Suit
    
    var description: String {
        return "\(rank.description)\(suit.rawValue)"
    }
}

func combinations(_ cards: [Card]) -> [[Card]] {
    var result: [[Card]] = []
    
    for i in 0..<cards.count {
        for j in (i+1)..<cards.count {
            for k in (j+1)..<cards.count {
                for l in (k+1)..<cards.count {
                    for m in (l+1)..<cards.count {
                        let combination = [cards[i], cards[j], cards[k], cards[l], cards[m]]
                        result.append(combination)
                    }
                }
            }
        }
    }
    
    return result
}

func checkFlush(_ cards: [Card]) -> Bool {
    let suits = Set(cards.map { $0.suit })
    return suits.count == 1
}

func checkStraight(_ cards: [Card]) -> Bool {
    let sortedRanks = cards.map { $0.rank.rawValue }.sorted()
    return Set(sortedRanks) == Set(sortedRanks.min()!...sortedRanks.max()!)
}

func checkStraightFlush(_ cards: [Card]) -> Bool {
    return checkFlush(cards) && checkStraight(cards)
}

func checkRoyalFlush(_ cards: [Card]) -> Bool {
    let royalRanks: Set<Rank> = [.ace, .jack, .queen, .king]
    return checkFlush(cards) && Set(cards.map { $0.rank }) == royalRanks
}

func deal() -> [Card] {
    var cards = [Card]()
    for _ in 1...5 {
        let randomIndex = Int.random(in: 0..<deck.count)
        cards.append(deck[randomIndex])
        deck.remove(at: randomIndex)
    }
    return cards
}

var combinations = [String]()

let hand = deal()
print("Your hand is:")
for card in hand {
    print(card.description)
}

if checkRoyalFlush(hand) {
    print("You have a royal flush!")
    combinations.append("Royal flush")
} else if checkStraightFlush(hand) {
    print("You have a straight flush!")
    combinations.append("Straight flush")
} else if checkFlush(hand) {
    print("You have a flush!")
    combinations.append("Flush")
} else if checkStraight(hand) {
    print("You have a straight!")
    combinations.append("Straight")
} else {
    print("Sorry, no winning combination this time.")
}

print("Your combinations are:")
for combination in combinations {
    print(combination)
}