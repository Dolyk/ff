Для каждой структуры определен метод performAction, который принимает значение перечисления CarAction или TruckAction и изменяет свойства структуры в соответствии с выбранным действием.

Создадим несколько экземпляров структур, применим к ним различные действия и поместим их в словарь.

var car1 = Car(brand: "Honda Civic", year: 2020, trunkVolume: 12.5, isEngineOn: false, areWindowsOpen: false, filledTrunkVolume: 0)
car1.performAction(action: .startEngine)

var car2 = Car(brand: "Toyota Camry", year: 2018, trunkVolume: 15.0, isEngineOn: false, areWindowsOpen: false, filledTrunkVolume: 0)
car2.performAction(action: .openWindows)

var truck1 = Truck(brand: "Volvo FH16", year: 2021, body
