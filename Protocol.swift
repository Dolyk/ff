Создание протокола Hotel и класса HotelAlfa:

protocol Hotel {
    init(roomCount: Int)
}

class HotelAlfa: Hotel {
    let roomCount: Int
    
    required init(roomCount: Int) {
        self.roomCount = roomCount
    }
}

Создание протокола GameDice и расширение типа Int:

protocol GameDice {
    var numberDice: Int { get }
}

extension Int: GameDice {
    var numberDice: Int {
        print("Выпало \(self) на кубике")
        return self
    }
}


Создание протокола с одним методом и 2 свойствами, подпись класса и реализация обязательного свойства:

protocol MyProtocol {
    var name: String { get }
    var age: Int? { get set }
    
    func myMethod()
}

class MyClass: MyProtocol {
    let name: String
    
    var age: Int?
    
    init(name: String) {
        self.name = name
    }
    
    func myMethod() {
        print("My method")
    }
}


Создание 2 протоколов со свойствами и методами и класса Company:

protocol CodeTime {
    var time: Int { get }
    var linesOfCode: Int { get }
    
    func writeCode(platform: String, numberOfSpecialist: Int)
}

protocol StopCoding {
    func stopCoding()
}

class Company {
    let numberOfProgrammers: Int
    let specializations: [String]
    
    init(numberOfProgrammers: Int, specializations: [String]) {
        self.numberOfProgrammers = numberOfProgrammers
        self.specializations = specializations
    }
}

extension Company: CodeTime {
    var time: Int {
        return 8
    }
    
    var linesOfCode: Int {
        return numberOfProgrammers * 100
    }
    
    func writeCode(platform: String, numberOfSpecialist: Int) {
        print("Разработка началась. Пишем код для \(platform) специалистами в количестве \(numberOfSpecialist)")
    }
}

extension Company: StopCoding {
    func stopCoding() {
        print("Работа закончена. Сдаю в тестирование")
    }
}

6. Пример использования:

let company = Company(numberOfProgrammers: 10, specializations: ["iOS", "Android", "Web"])

company.writeCode(platform: "iOS", numberOfSpecialist: 5)
company.stopCoding()

company.writeCode(platform: "Windows", numberOfSpecialist: 2) // "Разработка началась. П
