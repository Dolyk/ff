// Задача 1: сортировка массива с помощью замыкания
var array = [5, 3, 8, 1, 4, 7, 2, 6]

// Сортировка в одну сторону
array.sort { $0 < $1 }
print(array)

// Сортировка в обратную сторону
array.sort { $0 > $1 }
print(array)

// Задача 2: создание метода для сортировки массива по количеству букв в имени
func sortFriendsByLength(_ friends: [String]) -> [String] {
    return friends.sorted { $0.count < $1.count }
}

// Пример использования метода
let friends = ["Alex", "Bob", "Charlie", "David", "Eva", "Frank"]
let sortedFriends = sortFriendsByLength(friends)
print(sortedFriends)

// Задача 3: создание словаря
let dictionary: [Int: String] = [3: "Bob", 4: "Alex", 5: "Eva", 6: "David", 7: "Charlie", 8: "Frank"]

// Задача 4: функция для вывода ключа и значения по ключу
func printDictionaryValue(forKey key: Int, in dictionary: [Int: String]) {
    if let value = dictionary[key] {
        print("Key: \(key), Value: \(value)")
    } else {
        print("No value found for key \(key)")
    }
}

// Пример использования функции
printDictionaryValue(forKey: 5, in: dictionary)

// Задача 5: функция для проверки пустоты массивов и добавления значения, если массивы пустые
func checkArraysAndAddValueIfNeeded(_ array1: [String], _ array2: [Int]) -> ([String], [Int]) {
    var mutableArray1 = array1
    var mutableArray2 = array2
    
    if array1.isEmpty {
        mutableArray1.append("default value 1")
    }
    
    if array2.isEmpty {
        mutableArray2.append(0)
    }
    
    return (mutableArray1, mutableArray2)
}

// Пример использования функции
let array1 = ["a", "b", "c"]
let array2: [Int] = []

let checkedArrays = checkArraysAndAddValueIfNeeded(array1, array2)
print(checkedArrays)
